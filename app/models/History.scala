package models

import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.casbah.Imports._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import java.util.Date
import play.api.{Logger, Play}

/**
 * Created by stipe on 3.4.2014..
 */
object History {
  val collection = MongoManager.instance.db("history")

  def log(collectionName: String, action: String, who: String, data: MongoDBObject) {
    Future {
      val doc = MongoDBObject(
        "collectionName" -> collectionName,
        "action" -> action,
        "when" -> new Date(),
        "who" -> who,
        "data" -> data
      )

      val lastError = collection.insert(doc).getLastError
      if(lastError.ok() == false){
        Logger.error(s"Error while trying to log history data. Reason: ${lastError.getErrorMessage}")
      }
    }
  }
}
