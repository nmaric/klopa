package models

import java.util.Date
import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.casbah.Imports._
import scala.util.{Success, Failure, Try}
import play.api.libs.json.{JsArray, JsBoolean, JsString, Json}
import utils.Helper
import scala.collection.mutable

/**
 * Created by stipe on 03.03.14..
 */

case class User(username: String, password: String, firstName: String, lastName: String, email: String){
  var telephone: Option[String] = None
  var createdAt: Option[Date] = None
  var createdBy: Option[String] = None
  var updatedAt: Option[Date] = None
  var updatedBy: Option[String] = None
  var active: Boolean = true
  var roles: List[String] = List()

  def fullName() = s"$firstName $lastName"

  def hasRole(roleName: String): Boolean = {
    roles.contains(roleName)
  }

  def asMongoDBObject() = {
    val doc = MongoDBObject(
      "_id" -> username,
      "password" -> password,
      "firstName" -> firstName,
      "lastName" -> lastName,
      "email" -> email,
      "roles" -> roles,
      "active" -> active
    )

    if(telephone.isDefined) doc += ("telephone" -> telephone.get)
    if(createdAt.isDefined) doc += ("createdAt" -> createdAt.get)
    if(createdBy.isDefined) doc += ("createdBy" -> createdBy.get)
    if(updatedAt.isDefined) doc += ("updatedAt" -> updatedAt.get)
    if(updatedBy.isDefined) doc += ("updatedBy" -> createdBy.get)

    doc
  }

  def asJson() = {
    var json = Json.obj(
      "username" -> JsString(username),
      "firstName" -> JsString(firstName),
      "lastName" -> JsString(lastName),
      "email" -> JsString(email),
      "active" -> JsBoolean(active),
      "roles" -> JsArray(roles.map( elem => JsString(elem)))
    )

    if(telephone.isDefined) json = json + ("telephone" -> JsString(telephone.get))
    if(createdAt.isDefined) json = json + ("createdAt" -> JsString(Helper.formatDate(createdAt.get)))
    if(createdBy.isDefined) json = json + ("createdBy" -> JsString(createdBy.get))
    if(updatedAt.isDefined) json = json + ("updatedAt" -> JsString(Helper.formatDate(updatedAt.get)))
    if(updatedBy.isDefined) json = json + ("updatedBy" -> JsString(createdBy.get))

    json
  }
}


object User {
  val DUMMY_USER = new User("dummy", "dummy", "Dummy", "Dumm", "dummy@dummy.com")

  val collection = MongoManager.instance.db("users")

  //Create new user
  def create(user: User): Try[String] = {
    if (user.createdBy.isEmpty) {
      Failure(new Exception("User.createdBy must be defined!"))
    } else {

      if (user.createdAt.isEmpty) {
        user.createdAt = Some(new Date())
      }

      try{
        val data = user.asMongoDBObject()
        Helper.time("User.create(..)") {

          val lastError = collection.insert(data).getLastError
          if (lastError.ok()) {
            History.log("users","create", user.createdBy.get, data)
            Success(s"User ${user.username} has been created!")
          } else {
            Failure(lastError.getException)
          }
        }

      }catch{
        case ex: Throwable => Failure(ex)
      }

    }
  }

  //Find user by username
  def findByUsername(username: String): Option[User] = {
    if(username==DUMMY_USER.username){
      Some(DUMMY_USER)
    }else{
      Helper.time("User.findByUsername(..)") {
        val doc = collection.findOne(MongoDBObject("_id" -> username))
        if (doc.isDefined)
          Some(mongoToUser(doc.get))
        else
          None
      }
    }
  }

  def findAll(): List[User] = {
    Helper.time("User.findAll(..)") {
      val docs = collection.find()
      var list = List[User]()
      for (doc <- docs) {
        list = mongoToUser(doc) :: list
      }

      list
    }
  }

  def remove(username: String, updatedBy: String): Try[String] = {
    Helper.time("User.remove(..)") {
      val data = MongoDBObject("_id" -> username)
      val lastError = collection.update(
        data,
        $set("active" -> false, "updatedBy" -> updatedBy, "updatedAt" -> new Date())
      ).getLastError

      if (lastError.ok()) {
        History.log("users", "remove", updatedBy, data)
        Success(s"User $username has been marked as inactive!")
      } else {
        Failure(lastError.getException)
      }
    }
  }

  def update(user: User): Try[String] = {
    Helper.time("User.update(..)") {
      if (user.updatedBy.isEmpty) {
        Failure(new Exception("User.updatedBy must be defined!"))
      } else {

        if (user.updatedAt.isEmpty) {
          user.updatedAt = Some(new Date())
        }

        val data = user.asMongoDBObject()
        val lastError = collection.save(data).getLastError
        if (lastError.ok()) {
          History.log("users", "update", user.updatedBy.get, data)
          Success(s"User ${user.username} has been updated!")
        } else {
          Failure(lastError.getException)
        }
      }
    }
  }

  def update(username: String, data: Map[String, Any], updatedBy: String): Try[String] = {
    Helper.time("User.update(...)") {
      val query = MongoDBObject("_id" -> username)

      val fields = MongoDBObject()
      data.foreach(e => fields.put(e._1, e._2))
      fields.put("updatedBy", updatedBy)
      fields.put("updatedAt", new Date())

      try {
        val updateCount = collection.update(query, MongoDBObject("$set" -> fields), upsert = false).getN
        if (updateCount > 0) {
          History.log("users", "update", updatedBy, fields)
          Success(s"User $username has been updated!")
        } else {
          Failure(new Exception(s"Unknown user $username!"))
        }
      } catch {
        case ex: Throwable => Failure(ex)
      }
    }
  }


  //Convert MongoDB object to User case class
  def mongoToUser(doc: MongoDBObject): User = {
    val user = User(
      doc.get("_id").orNull.asInstanceOf[String],
      doc.get("password").orNull.asInstanceOf[String],
      doc.get("firstName").orNull.asInstanceOf[String],
      doc.get("lastName").orNull.asInstanceOf[String],
      doc.get("email").orNull.asInstanceOf[String]
    )

    user.telephone = doc.getAs[String]("telephone")
    user.createdAt = doc.getAs[Date]("createdAt")
    user.createdBy = doc.getAs[String]("createdBy")
    user.updatedAt = doc.getAs[Date]("updatedAt")
    user.updatedBy = doc.getAs[String]("updatedBy")
    user.active = doc.getAsOrElse[Boolean]("active", false)
    user.roles = doc.getAsOrElse[MongoDBList]("roles", MongoDBList.empty).map(e => e.asInstanceOf[String]).toList

    user
  }



  /**
   * Authenticate a User.
   */
  def authenticate(username: String, password: String): Option[User] = {
    if(username == null || password == null || username.trim == "" || password.trim == "")
      None
    else{
      findByUsername(username) match {
        case Some(user) if(user.active && (user.password == password)) =>
          Some(user)
        case _ =>
          None
      }
    }
  }
}
