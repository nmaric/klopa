package controllers

import play.api.libs.json.JsArray
import models.User
import scala.util.{Failure, Success}

/**
 * Created by stipe on 2.4.2014..
 */
object UserController extends AppController{

  def list = IsAuthenticated { user => request =>
    val json = JsArray( User.findAll().map(u => u.asJson()) )
    Ok(json)
  }

  def findByUsername(username: String) = IsAuthenticated { user => request =>
    User.findByUsername(username) match{
      case Some(user) =>
        Ok(user.asJson())
      case None =>
        NotFound(s"Unknown user $username!")
    }
  }

  def create = IsAuthenticated(parse.json) { user => request =>
    val username = (request.body \ "username").as[String]
    val firstName = (request.body \ "firstName").as[String]
    val lastName = (request.body \ "lastName").as[String]
    val password = (request.body \ "password").as[String]
    val email = (request.body \ "email").as[String]

    val newUser = new User(username, password, firstName, lastName, email)
    newUser.createdBy = Some(user.username)
    newUser.telephone = (request.body \ "telephone").asOpt[String]
    newUser.roles = ( (request.body \ "roles").asOpt[List[String]] ).getOrElse(List())

    User.create(newUser) match {
      case Success(msg) =>
        Ok(msg)
      case Failure(ex) =>
        BadRequest(ex.getMessage)
    }
  }

  def remove(username: String) = IsAuthenticated { user => request =>
    User.remove(username, updatedBy = user.username) match {
      case Success(msg) =>
        Ok(msg)
      case Failure(ex) =>
        BadRequest(ex.getMessage)
    }
  }

  def update(username: String) = IsAuthenticated(parse.json){ user => request =>

    val updateData = scala.collection.mutable.Map[String, Any]()

    val passwordOption = (request.body \ "password").asOpt[String]
    if(passwordOption.isDefined) updateData.put("password", passwordOption.get)

    val firstNameOption = (request.body \ "firstName").asOpt[String]
    if(firstNameOption.isDefined) updateData.put("firstName", firstNameOption.get)

    val lastNameOption = (request.body \ "lastName").asOpt[String]
    if(lastNameOption.isDefined) updateData.put("lastName", lastNameOption.get)

    val emailOption = (request.body \ "email").asOpt[String]
    if(emailOption.isDefined) updateData.put("email", emailOption.get)

    val telephoneOption = (request.body \ "telephone").asOpt[String]
    if(telephoneOption.isDefined) updateData.put("telephone", telephoneOption.get)

    val activeOption = (request.body \ "active").asOpt[Boolean]
    if(activeOption.isDefined) updateData.put("active", activeOption.get)

    val rolesOption = (request.body \ "roles").asOpt[List[String]]
    if(rolesOption.isDefined) updateData.put("roles", rolesOption.get)

    User.update(username, updateData.toMap, updatedBy = user.username) match {
      case Success(msg) =>
        Ok(msg)
      case Failure(ex) =>
        BadRequest(ex.getMessage)
    }
  }

}
