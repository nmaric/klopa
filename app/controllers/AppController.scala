package controllers

import play.api.mvc._
import utils.{TemplateHelper, ApplicationConfig}

/**
 * Created by stipe on 03.03.14..
 */
trait AppController extends Controller with Secured{
  val config = ApplicationConfig
  def message(key: String) = TemplateHelper.message(key)

}
