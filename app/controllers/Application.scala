package controllers

import play.api.mvc._
import views.html
import play.api.data.Form
import play.api.data.Forms._
import models.User

object Application extends AppController {

  def index = IsAuthenticated { user => implicit request =>
    Ok(views.html.index(user))
  }

  /**
   * Authentication related actions
   */
  val loginForm = Form(
    tuple(
      "username" -> text,
      "password" -> text,
      "redirect" -> text
    ) verifying (message("login.error"), result => result match {
      case (username, password, redirect) => User.authenticate(username, password).isDefined
    })
  )

  def login(redirect: String) = Action { implicit request =>
    Ok(html.login(loginForm.bind(Map("redirect" -> redirect))))
  }

  def logout = IsAuthenticated { user => implicit request =>
    Redirect(routes.Application.login("/")).withNewSession
  }

  def authenticate = Action { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(html.login(formWithErrors))
      },
      user => {
        Redirect(user._3).withSession("username" -> user._1)
      }
    )
  }

}