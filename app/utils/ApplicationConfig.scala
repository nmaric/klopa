package utils

import com.typesafe.config.ConfigFactory
import play.api.i18n.Lang

/**
 * Created by stipe on 03.03.14..
 */
object ApplicationConfig extends Config

trait Config {
  val config = ConfigFactory.load

  lazy val environment = System.getProperty("environment","dev").toLowerCase
  lazy val devEnvironment: Boolean = ("dev" == environment || "development" == environment)
  lazy val qaEnvironment: Boolean = ("qa" == environment)
  lazy val testEnvironment: Boolean = ("test" == environment)
  lazy val prodEnvironment: Boolean = ("prod" == environment || "production" == environment)

  lazy val environmentAsLongString: String = environment match {
    case "dev" | "development"  => "DEVELOPMENT"
    case "qa"   => "QA"
    case "test" => "TEST"
    case "prod" | "production" => "PRODUCTION"
  }

  lazy val environmentAsShortString: String = environment match {
    case "dev"  => "DEV"
    case "qa"   => "QA"
    case "test" => "TEST"
    case "prod" => "PROD"
  }

  lazy val defaultLang = Lang(config.getString("default.lang"))
}
