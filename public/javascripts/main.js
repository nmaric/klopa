var etna = {

    notifications: {
        info: function(message){
            etna.notifications.notify(message, "info");
        },

        error: function(message){
            etna.notifications.notify(message, "danger");
        },

        success: function(message){
            etna.notifications.notify(message, "success");
        },

        warning: function(message){
            etna.notifications.notify(message, "warning");
        },

        notify: function(message, type){
            $.bootstrapGrowl(message, {
                  ele: 'body',
                  type: type,
                  offset: {from: 'top', amount: 20}, // 'top', or 'bottom'
                  align: 'right', // ('left', 'right', or 'center')
                  width: 250, // (integer, or 'auto')
                  delay: 4000,
                  allow_dismiss: true,
                  stackup_spacing: 10 // spacing between consecutively stacked growls.
            });
        }
    }
};