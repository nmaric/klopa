name := "etna-play-template"

organization  := "hr.etna"

version := "1.0-SNAPSHOT"

scalaVersion  := "2.10.3"

resolvers ++= Seq(
  "jasperreports repo" at "http://jasperreports.sourceforge.net/maven2/"
)

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  "org.webjars"               %% "webjars-play"              % "2.2.1-2",
  "org.webjars"               %  "bootstrap"                 % "3.1.1",
  "org.webjars"               %  "angularjs"                 % "1.2.13",
  "org.webjars"               %  "jquery"                    % "2.1.0-2",
  "org.webjars"               %  "font-awesome"              % "4.0.3",
  "joda-time"                 %  "joda-time"                 % "2.3",
  "org.mongodb"               %  "casbah_2.10"               % "2.7.0"
)     

play.Project.playScalaSettings

templatesImport ++= Seq("utils.TemplateHelper._")
